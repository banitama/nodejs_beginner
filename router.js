function route (handle,pathname,res,postData) {
	console.log("Routing request for " + pathname);
	if (typeof handle[pathname] == 'function') {
		handle[pathname](res,postData);
	} else {
		console.log ( "No request handler for " + pathname);
		return "404 Not Found";
	}
}
exports.route = route;